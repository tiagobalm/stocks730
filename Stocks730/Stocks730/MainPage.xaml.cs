﻿using OxyPlot;
using OxyPlot.Axes;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace Stocks730
{
    public partial class MainPage : ContentPage
    {

        Boolean SevenQPressed = true;
        Boolean ThirtyQPressed = false;
        SelectableObservableCollection<Company> items;
        Company[] companies;

        public MainPage()
        {
            InitializeComponent();

            initializeCompaniesArray();

            items = new SelectableObservableCollection<Company>(companies);
            companiesList.ItemsSource = items;

            changeButtonBackground();
        }

        private void SevenQ_Clicked(object sender, EventArgs e)
        {
            SevenQPressed = !SevenQPressed;
            if (SevenQPressed && ThirtyQPressed)
                ThirtyQPressed = !ThirtyQPressed;
            changeButtonBackground();
        }

        private void ThirtyQ_Clicked(object sender, EventArgs e)
        {
            ThirtyQPressed = !ThirtyQPressed;
            if (SevenQPressed && ThirtyQPressed)
                SevenQPressed = !SevenQPressed;
            changeButtonBackground();
        }

        private void changeButtonBackground()
        {
            if (SevenQPressed)
                SevenQ.BackgroundColor = Color.FromHex("#0099ff");
            else
                SevenQ.BackgroundColor = Color.White;

            if (ThirtyQPressed)
                ThirtyQ.BackgroundColor = Color.FromHex("#0099ff");
            else
                ThirtyQ.BackgroundColor = Color.White;
        }

        private async void Graph_Clicked(object sender, EventArgs e)
        {
            IEnumerable<Company> selectedItems = items.SelectedItems;
            List<List<DataPoint>> entries = new List<List<DataPoint>>();

            if(selectedItems.Count() == 0)
            {
                await DisplayAlert("Error", "Please select at least one company.", "Close");
                return;
            }

            string[] colors = new string[] { "266489", "84C345" };
            int i = 0;
            double maxValue = 0;

            DateTime today = DateTime.Now;
            DateTime beginningDate;
            if (SevenQPressed)
                beginningDate = today.AddDays(-7.0);
            else
                beginningDate = today.AddDays(-30.0);

            DateTime maximumDate = beginningDate;

            List<string> selectedCompaniesSymbols = new List<string>();

            foreach (Company item in selectedItems)
            {

                CompanyQuote[] response = await App.RestService.getCompanyHistory(item.Symbol, beginningDate.ToString("yyyyMMdd"));
                List<DataPoint> tempEntries = new List<DataPoint>();
                selectedCompaniesSymbols.Add(item.Symbol);
                beginningDate = today;

                foreach (CompanyQuote companyQuote in response)
                {
                    if (companyQuote.close > maxValue)
                        maxValue = companyQuote.close;

                    if (companyQuote.tradingDay.CompareTo(maximumDate) > 0)
                        maximumDate = companyQuote.tradingDay;

                    if (companyQuote.tradingDay.CompareTo(beginningDate) <= 0)
                        beginningDate = companyQuote.tradingDay;

                    tempEntries.Add(new DataPoint(DateTimeAxis.ToDouble(companyQuote.tradingDay), companyQuote.close));
                }

                entries.Add(tempEntries);
                i++;
            }

            await Navigation.PushAsync(new ChartPage(entries, maxValue, beginningDate, maximumDate, selectedCompaniesSymbols));
        }

        private void initializeCompaniesArray()
        {
            companies = new[]
            {
                new Company()
                {
                    Name = "Apple",
                    CurrentQuote = "2.3",
                    Symbol = "AAPL"
                },
                new Company()
                {
                    Name = "Microsoft Corporation",
                    CurrentQuote = "1.3",
                    Symbol = "MSFT"
                },
                new Company()
                {
                    Name = "Amazon.com Inc.",
                    CurrentQuote = "0.5",
                    Symbol = "AMZN"
                },
                new Company()
                {
                    Name = "Alphabet Inc.",
                    CurrentQuote = "1.0",
                    Symbol = "GOOGL"
                },
                new Company()
                {
                    Name = "Facebook, Inc.",
                    CurrentQuote = "0",
                    Symbol = "FB"
                },
                new Company()
                {
                    Name = "Intel Corportation",
                    CurrentQuote = "0",
                    Symbol = "INTC"
                },
                new Company()
                {
                    Name = "Cisco Systems, Inc.",
                    CurrentQuote = "0",
                    Symbol = "CSCO"
                },
                new Company()
                {
                    Name = "Comcast Corporation",
                    CurrentQuote = "0",
                    Symbol = "CMCSA"
                },
                new Company()
                {
                    Name = "Pepsico, Inc.",
                    CurrentQuote = "0",
                    Symbol = "PEP"
                },
                new Company()
                {
                    Name = "Amgen Inc.",
                    CurrentQuote = "0",
                    Symbol = "AMGN"
                },
                new Company()
                {
                    Name = "Netflix, Inc.",
                    CurrentQuote = "0",
                    Symbol = "NFLX"
                },
                new Company()
                {
                    Name = "Adobe Inc.",
                    CurrentQuote = "0",
                    Symbol = "ADBE"
                },
                new Company()
                {
                    Name = "Paypal Holdings, Inc.",
                    CurrentQuote = "0",
                    Symbol = "PYPL"
                },
                new Company()
                {
                    Name = "NVIDIA Corporation",
                    CurrentQuote = "0",
                    Symbol = "NVDA"
                },
                new Company()
                {
                    Name = "Booking Holdings Inc.",
                    CurrentQuote = "0",
                    Symbol = "BKNG"
                },
                new Company()
                {
                    Name = "Starbucks Corporation",
                    CurrentQuote = "0",
                    Symbol = "SBUX"
                },
                new Company()
                {
                    Name = "QUALCOMM Incorporated",
                    CurrentQuote = "0",
                    Symbol = "QCOM"
                }
            };
        }
    }
}
