﻿using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;

namespace Stocks730
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChartPage : ContentPage
	{
		public ChartPage (List<List<DataPoint>> entries, double maxValue, DateTime beginningDate, DateTime maximumDate, List<string> selectedCompaniesSymbols)
		{
			InitializeComponent ();

            PlotModel plotModel = new PlotModel { Title = "Quotes" };
            plotModel.Axes.Add(new DateTimeAxis { Position = AxisPosition.Bottom, Minimum = DateTimeAxis.ToDouble(beginningDate), Maximum = DateTimeAxis.ToDouble(maximumDate) , StringFormat = "MM-dd"});
            plotModel.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Minimum = 0 });

            plotModel.Series.Add(new AreaSeries
            {
                Title = selectedCompaniesSymbols.ElementAt(0),
                StrokeThickness = 1,
                Color = OxyColors.LightSkyBlue,
                MarkerType = MarkerType.Circle,
                MarkerSize = 4,
                MarkerStroke = OxyColors.White,
                MarkerFill = OxyColors.LightSkyBlue,
                MarkerStrokeThickness = 1,
                ItemsSource = entries.ElementAt(0).ToArray()
            });

            if(entries.Count() == 2)
            {
                plotModel.Series.Add(new AreaSeries
                {
                    Title = selectedCompaniesSymbols.ElementAt(1),
                    StrokeThickness = 1,
                    Color = OxyColors.ForestGreen,
                    MarkerType = MarkerType.Circle,
                    MarkerSize = 4,
                    MarkerStroke = OxyColors.White,
                    MarkerFill = OxyColors.ForestGreen,
                    MarkerStrokeThickness = 1,
                    ItemsSource = entries.ElementAt(1).ToArray()

                });
            }

            Chart.Model = plotModel;
        }
	}
}