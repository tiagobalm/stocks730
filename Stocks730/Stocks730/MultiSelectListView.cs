﻿using Xamarin.Forms;

namespace Stocks730
{
    public static class MultiSelectListView
    {

        public static int selectedItems = 0;

        public static readonly BindableProperty IsMultiSelectProperty =
            BindableProperty.CreateAttached("IsMultiSelect", typeof(bool), typeof(ListView), false, propertyChanged: OnIsMultiSelectChanged);

        public static bool GetIsMultiSelect(BindableObject view) => (bool)view.GetValue(IsMultiSelectProperty);

        public static void SetIsMultiSelect(BindableObject view, bool value) => view.SetValue(IsMultiSelectProperty, value);

        private static void OnIsMultiSelectChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var listView = bindable as ListView;
            if (listView != null)
            {
                // always remove event
                listView.ItemSelected -= OnItemSelected;

                // add the event if true
                if (true.Equals(newValue))
                {
                    listView.ItemSelected += OnItemSelected;
                }
            }
        }

        private static void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as SelectableItem;
            if (item != null)
            {
                if (!item.IsSelected && selectedItems < 2)
                {
                    // toggle the selection property
                    item.IsSelected = true;
                    selectedItems += 1;
                }
                else if (item.IsSelected)
                {
                    // toggle the selection property
                    item.IsSelected = false;
                    selectedItems -= 1;
                }
            }

            // deselect the item
            ((ListView)sender).SelectedItem = null;
        }
    }
}
