﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Stocks730
{
    public class RestService
    {
        HttpClient httpClient;

        public RestService()
        {
            httpClient = new HttpClient();
        }

        public async Task<CompanyQuote[]> getCompanyHistory(string companySymbol, string startDate)
        {
            string uri = "https://marketdata.websol.barchart.com/getHistory.json?apikey=75e7b1cb388afc0428431cd03cccd1ff&symbol=";
            uri += companySymbol;
            uri += "&type=daily&startDate=";
            uri += startDate;

            var request = new HttpRequestMessage();
            request.RequestUri = new Uri(uri);
            request.Method = HttpMethod.Get;
            request.Headers.Add("Accept", "application/json");

            HttpResponseMessage response = await httpClient.SendAsync(request);

            if(response.StatusCode == HttpStatusCode.OK)
            {
                var responseString = await response.Content.ReadAsStringAsync();
                Newtonsoft.Json.Linq.JObject responseObject = Newtonsoft.Json.Linq.JObject.Parse(responseString);

                CompanyQuote[] answer = JsonConvert.DeserializeObject<CompanyQuote[]>(responseObject["results"].ToString());
                return answer;
            }

            return null;
        }
       

    }
}
