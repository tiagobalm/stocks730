﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Stocks730
{
    public partial class App : Application
    {

        static RestService restService;

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage())
            {
                Title = "Stocks 730"
            };
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public static RestService RestService
        {
            get {

                if(restService == null)
                {
                    restService = new RestService();
                }

                return restService;
            }
        }
    }
}
