﻿namespace Stocks730
{
    class Company
    {

        public string Name { get; set; }

        public string CurrentQuote { get; set; }

        public string Symbol { get; set; }

    }
}
