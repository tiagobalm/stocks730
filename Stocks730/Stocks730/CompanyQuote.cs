﻿using System;

namespace Stocks730
{
    public class CompanyQuote
    {

        public string symbol { get; set; }
        public DateTime timestamp { get; set; }
        public DateTime tradingDay { get; set; }
        public double open { get; set; }
        public double high { get; set; }
        public double low { get; set; }
        public double close { get; set; }
        public double volume { get; set; }
        public string openInterest { get; set; }
    }
}
